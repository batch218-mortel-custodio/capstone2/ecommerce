const jwt = require("jsonwebtoken");
const secret = "ECommerceAPI";

module.exports.createAccessToken = (user) =>{
	//payload
	const data = {
		id: user._id,
		firstName: user.firstName,
		email: user.email,
		isAdmin: user.isAdmin
	}
							// expiration data/callback function
	return jwt.sign(data, secret, {});
}
							//parameters
module.exports.verify = (request, response, next) => {
				//arguments
	//get jwt from postman
	let token = request.headers.authorization;

	if(typeof token !== "undefined"){
		console.log('auth.js tokennnnn', token);
		//remove unnecessary charactes from the token <check console
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data)=>{
			if(error){
				return response.send({
					auth: "Failed"
				})
			}
			else{
				next();
			}
		})
	}
	else{
		return null;
	}
}

// to decode the user details from the token

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

	}
	return jwt.verify(token, secret, (error, data) =>{
		if(error){
			return null;
		}
		else{
			return jwt.decode(token, {complete: true}).payload;
		}
	})
}