const Product = require("../models/product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



// CREATE PRODUCT - ADMIN ONLY (SESSION 2)
module.exports.addProduct = (data) => {
		let newProduct = new Product({
			name: data.name,
			description: data.description,
			price: data.price,
			initialInventory: data.initialInventory
		});
		return newProduct.save().then((newProduct, error) => {
			if(error){
				return error;
			}
			else{
				return newProduct;
			}
		})
}


//RETRIEVE ALL ACTIVE PRODUCT - ALL USERS (SESSION 2)
module.exports.getActiveProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


// RETRIEVE SINGLE PRODUCT BY ID - ALL USERS (SESSION 3)
module.exports.getProduct = (id) => {
	return Product.findById(id).then(result => {
		return result;
	})
}








// UPDATE PRODUCT INFO BY ID - ADMIN ONLY (SESSION 3)
module.exports.updateProduct = (id, data) => {
		return Product.findByIdAndUpdate(id, 
				{
					name: data.name,
					description: data.description,
					price: data.price,
					initialInventory: data.initialInventory
				}
			).then((updatedProduct, error) =>{
				if(error){
					return false;
				}
				else{
					return updatedProduct;
				}
			})
}


// ARCHIVE A PRODUCT BY ID - ADMIN ONLY (SESSION 3)
module.exports.archiveProduct = (id) => {
		return Product.findByIdAndUpdate(id, 
				{
					isActive: false
				}
			).then((updatedProduct, error) =>{
				if(error){
					return false
				}
				else{
					return true
				}
			})
}

module.exports.activateProduct = (id) => {
		return Product.findByIdAndUpdate(id, 
				{
					isActive: true
				}
			).then((updatedProduct, error) =>{
				if(error){
					return false
				}
				else{
					return true
				}
			})
}

// // ARCHIVE A PRODUCT BY ID - ADMIN ONLY (SESSION 3)
// module.exports.archiveProduct = (productId, update) => {
// 	if(update.isAdmin == true){
// 		return Product.findByIdAndUpdate(productId, 
// 				{
// 					isActive: false
// 				}
// 			).then((updatedProduct, error) =>{
// 				if(error){
// 					return false
// 				}
// 				else{
// 					return "This product is now removed from active product/s."
// 				}
// 			})
// 	}
// 	else{
// 		let message = Promise.resolve("User must be ADMIN to access this.");
// 		return message.then((value) => {return value})
// 	}
// }



