const User = require("../models/user.js");
const Cart = require("../models/cart.js");
const Product = require("../models/product.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// Will REGISTER new users ONLY WHEN email and/or mobile number DOESNT EXIST in the DATABASE (SESSION 1)
module.exports.registerUser = (reqBody) => { 
			let newUser = new User({	
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNo: reqBody.mobileNo
			})

			return newUser.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					//CREATE AN EMPTY CART HERE

					let newCart = new Cart({
						userId: user._id.toString()
					});
					console.log(user);
					console.log(newCart);
					return newCart.save();

				}
			})
}

// REGISTER NEW ADMIN (SESSION 1)

module.exports.registerAdmin = (reqBody) => {
	return User.find({$or:[{email:reqBody.email},{mobileNo:reqBody.mobileNo}]}).then(result => {
		if(result.length > 0){
			return "This email/mobile number currently exists. Please use another email and mobile number to register.";
		}
		else{
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNo: reqBody.mobileNo,
				isAdmin: true
			})
			return newUser.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return "Hello new admin!";
				}
			})
		}
	})
};

// EXISTING USER/S can login using their EMAIL or their MOBILE NUMBER (SESSION 1)
// return User.findOne({$or:[{email:reqBody.email},{mobileNo:reqBody.mobileNo}]})

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
				if (isPasswordCorrect){
					return {access: auth.createAccessToken(result)};
				}
				else{
					return false;
				}
		}
	})
}

module.exports.checkEmail = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false;
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result;
		}
	})
};

//RETRIEVE USER DETAILS BY ID (SESSION 4)

/*module.exports.getProfile = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			details.password = "";
			return details;
		}
	})
}
*/
/*
// USER CHECKOUT - NON-ADMIN (SESSION 4)
module.exports.order = async (data) => {
// if user is not an Admin, proceed to the next IF
	console.log(data);
	if(data.isAdmin == false){
		let productActive = [];
		for (let index = 0; index < data.products.length; index++){
			await Product.findById(data.products[index].productId).then(product => {
			productActive.push(product.isActive);
			});
		};
	// if ALL products to be purchased are Active, proceed to the next IF
			let isTrue = (value) => value == true;
			if (productActive.every(isTrue) == true){
				let index = 0; 
				let checkInventory = [];
				while(index < data.products.length){
					inventorySold = await Product.findById(data.products[index].productId).then(product => {
						return product.inventorySold;
					});

					initialInventory = await Product.findById(data.products[index].productId).then(product => {
						return product.initialInventory;
					});

					inventorySoldUpdated = inventorySold + data.products[index].quantity;

					if (inventorySoldUpdated <= initialInventory){
						checkInventory.push(true);
					}
					else{
						checkInventory.push(false);
					}
					index++;
				};
			// if ALL inventory of the products chosen is healthy if the purchase should proceed, continue to push all the products with their respective quantities/orders
						let isInventoryGood = (value) => value == true;
						if (checkInventory.every(isInventoryGood) == true){
							
							let index = 0;
							while(index < data.products.length){

								inventorySold = await Product.findById(data.products[index].productId).then(product => {
									return product.inventorySold;
								});

								initialInventory = await Product.findById(data.products[index].productId).then(product => {
									return product.initialInventory;
								});

								inventorySoldUpdated = inventorySold + data.products[index].quantity;

								productPrice = await Product.findById(data.products[index].productId).then(product => {
									return product.price;
								});
								productName = await Product.findById(data.products[index].productId).then(product => {
									return product.name;
								});
								totalamount = productPrice * data.products[index].quantity;

								isUserUpdated = await User.findById(data.userId).then(user => {

									user.orders.push({products: {productId: data.products[index].productId, productName: productName, quantity: data.products[index].quantity, price: productPrice}, totalAmount: totalamount});

									return user.save().then((user,error) => {
										if(error){
											return false;
										}
										else{
											return true;
										};
									});
								});

								isProductUpdated  = await Product.findById(data.products[index].productId).then(product => {

									product.orders.push({userId: data.userId, quantity: data.products[index].quantity});
									product.inventorySold = inventorySoldUpdated;

									return product.save().then((product, error) =>{
										if(error){
											return false;
										}
										else{
											return true;
										}
									});
								});

								index++;

								}

						}
						else{
							return "Current inventory of one of the products chosen are insufficient. Please try again."
						};
			
			}
			else{
				return "The product or one of the product is inactive. Please try another product."
			}
		}
		else{
			return "Product/s can only be purchased by non-Admin users."
		}
};
*/

// MAKE USER AS ADMIN - ADMIN ONLY (STRETCH GOAL)
module.exports.makeUserAsAdmin = (userId, update) => {
	if(update.isAdmin == true){
		return User.findByIdAndUpdate(userId, 
				{
					isAdmin: true
				}
			).then((updatedInfo, error) =>{
				if(error){
					return false
				}
				else{
					return "This user is now an admin."
				}
			})
	}
	else{
		let message = Promise.resolve("User must be ADMIN to access this.");
		return message.then((value) => {return value})
	}
};



//RETRIEVE AUTHENTICATED USER'S ORDERS BY ID (STRETCH GOAL)

module.exports.retrieveOrders = (data) => {
	if(data.isAdmin == false){
		return User.findById(data.userId).then((details, err) => {
			if(err){
				console.log(err);
				return false;
			}
			else{
				return details.orders;
			}
		})
	}
	else{
		return false;
	}
}


// RETRIEVE ALL ORDERS - ADMIN ONLY (STRETCH GOAL)
module.exports.getAllOrders = (retrieve) => {
	if(retrieve.isAdmin == true){
		return User.find({isAdmin: false},{firstName: 0, lastName: 0, email: 0, price: 0, password: 0, isAdmin: 0, mobileNo: 0}).then((result, error) =>{
				if(error){
					return false;
				}
				else{
					return result;
				}
			})
	}
	else{
		let message = Promise.resolve("User must be ADMIN to access this.");
		return message.then((value) => {return value})
	}
}


