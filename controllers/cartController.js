const Product = require("../models/product.js");
const User = require("../models/user.js");
const Cart = require("../models/cart.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

//////////////CART CART CART//////////////////////////////////////


//RETRIEVE CART (if any)
module.exports.retrieveCart = (data) => {
	if(data.isAdmin == false){
		return Cart.findOne({userId: data.userId}).then((result, error)=>{
			if(error){
				console.log(error);
				return false;
			}
			else{
				return result.orders;
			}
		})
	}
	else{
		return false;
	}
};


//ADD ONLY TO CART / MODIFY QUANTITY (STRETCH GOAL)

module.exports.addToCart = async (data) => {
	console.log('dataaaa sa addtoCart controller', data);
	if(data.isAdmin == false){
		let productActive = [];
			await Product.findById(data.productId).then(product => {
			productActive.push(product.isActive);
			});
	// if ALL products to be purchased are Active, proceed to the next IF
			let isTrue = (value) => value == true;
			if (productActive.every(isTrue) == true){

				let checkInventory = [];

					inventorySold = await Product.findById(data.productId).then(product => {
						return product.inventorySold;
					});

					initialInventory = await Product.findById(data.productId).then(product => {
						return product.initialInventory;
					});

					inventorySoldUpdated = inventorySold + data.quantity;

					if (inventorySoldUpdated <= initialInventory){
						checkInventory.push(true);
					}
					else{
						checkInventory.push(false);
					}
			// if ALL inventory of the products chosen is healthy should the purchase proceed, continue to push all the products with their respective quantities/orders
						let isInventoryGood = (value) => value == true;
						if (checkInventory.every(isInventoryGood) == true){
							

								inventorySold = await Product.findById(data.productId).then(product => {
									return product.inventorySold;
								});

								initialInventory = await Product.findById(data.productId).then(product => {
									return product.initialInventory;
								});

								inventorySoldUpdated = inventorySold + data.quantity;

								productPrice = await Product.findById(data.productId).then(product => {
									return product.price;
								});
								productName = await Product.findById(data.productId).then(product => {
									return product.name;
								});
								subTotal = productPrice * data.quantity;

								await Cart.findOne({userId: data.userId}).then(cart => {

									let count = cart.orders.map(post => post.productId).indexOf(data.productId);

									// if current product is not similar with the original cart, PUSH
									if(count === -1){

										cart.orders.push({productId: data.productId, productName: productName, quantity: data.quantity, price: productPrice, subTotal: subTotal});
										return cart.save();
									}
									// if not, splice it first, then push
									else{
										cart.orders.splice(count, 1);
										cart.orders.push({productId: data.productId, productName: productName, quantity: data.quantity, price: productPrice, subTotal: subTotal});
										return cart.save();

									}

									//return cart.save();

								});

								// CLEAN THIS UP!
								await Cart.findOne({userId: data.userId}).then(cart =>{
									let subTotalList = [];
									for (let index = 0; index < cart.orders.length; index++){
										subTotalList.push(cart.orders[index].subTotal)
									};


									let total = subTotalList.reduce((x, y) => {
									  return x + y;
									}, 0);

									
									cart.totalAmount = total;
				
									console.log("totaaaaaal", total);


									//return cart.save();
									return cart.save();

								});
									
								return true;
						}
						else{
							return false;
						};
			
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
};


module.exports.removeFromCart = async (data) => {
	console.log('dataaaa', data);
	if(data.isAdmin == false){
		let productActive = [];
			await Product.findById(data.productId).then(product => {
			productActive.push(product.isActive);
			});
	// if ALL products to be purchased are Active, proceed to the next IF
			let isTrue = (value) => value == true;
			if (productActive.every(isTrue) == true){

				let checkInventory = [];

					inventorySold = await Product.findById(data.productId).then(product => {
						return product.inventorySold;
					});

					initialInventory = await Product.findById(data.productId).then(product => {
						return product.initialInventory;
					});

					inventorySoldUpdated = inventorySold + data.quantity;

					if (inventorySoldUpdated <= initialInventory){
						checkInventory.push(true);
					}
					else{
						checkInventory.push(false);
					}
			// if ALL inventory of the products chosen is healthy should the purchase proceed, continue to push all the products with their respective quantities/orders
						let isInventoryGood = (value) => value == true;
						if (checkInventory.every(isInventoryGood) == true){
							

								// inventorySold = await Product.findById(data.productId).then(product => {
								// 	return product.inventorySold;
								// });

								// initialInventory = await Product.findById(data.productId).then(product => {
								// 	return product.initialInventory;
								// });

								// inventorySoldUpdated = inventorySold + data.quantity;

								// productPrice = await Product.findById(data.productId).then(product => {
								// 	return product.price;
								// });
								// productName = await Product.findById(data.productId).then(product => {
								// 	return product.name;
								// });
								// subTotal = productPrice * data.quantity;

								await Cart.findOne({userId: data.userId}).then(cart => {

									let count = cart.orders.map(post => post.productId).indexOf(data.productId);

									cart.orders.splice(count, 1);

									return cart.save();

								});

								// CLEAN THIS UP!
								await Cart.findOne({userId: data.userId}).then(cart =>{
									let subTotalList = [];
									for (let index = 0; index < cart.orders.length; index++){
										subTotalList.push(cart.orders[index].subTotal)
									};


									let total = subTotalList.reduce((x, y) => {
									  return x + y;
									}, 0);

									
									cart.totalAmount = total;
				
									console.log("totaaaaaal", total);


									//return cart.save();
									return cart.save();

								});
									
								return true;
						}
						else{
							return false;
						};
			
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
};

// //ADD ONLY TO CART / MODIFY QUANTITY (STRETCH GOAL)

// module.exports.addToCart = async (data) => {
// 	console.log(data);
// 	if(data.isAdmin == false){
// 		let productActive = [];
// 		for (let index = 0; index < data.products.length; index++){
// 			await Product.findById(data.products[index].productId).then(product => {
// 			productActive.push(product.isActive);
// 			});
// 		};
// 	// if ALL products to be purchased are Active, proceed to the next IF
// 			let isTrue = (value) => value == true;
// 			if (productActive.every(isTrue) == true){
// 				let index = 0; 
// 				let checkInventory = [];
// 				while(index < data.products.length){
// 					inventorySold = await Product.findById(data.products[index].productId).then(product => {
// 						return product.inventorySold;
// 					});

// 					initialInventory = await Product.findById(data.products[index].productId).then(product => {
// 						return product.initialInventory;
// 					});

// 					inventorySoldUpdated = inventorySold + data.products[index].quantity;

// 					if (inventorySoldUpdated <= initialInventory){
// 						checkInventory.push(true);
// 					}
// 					else{
// 						checkInventory.push(false);
// 					}
// 					index++;
// 				};
// 			// if ALL inventory of the products chosen is healthy should the purchase proceed, continue to push all the products with their respective quantities/orders
// 						let isInventoryGood = (value) => value == true;
// 						if (checkInventory.every(isInventoryGood) == true){
							
// 							let index = 0;
// 							while(index < data.products.length){

// 								inventorySold = await Product.findById(data.products[index].productId).then(product => {
// 									return product.inventorySold;
// 								});

// 								initialInventory = await Product.findById(data.products[index].productId).then(product => {
// 									return product.initialInventory;
// 								});

// 								inventorySoldUpdated = inventorySold + data.products[index].quantity;

// 								productPrice = await Product.findById(data.products[index].productId).then(product => {
// 									return product.price;
// 								});
// 								productName = await Product.findById(data.products[index].productId).then(product => {
// 									return product.name;
// 								});
// 								subTotal = productPrice * data.products[index].quantity;

// 								await Cart.findOne({userId: data.userId}).then(cart => {

// 									let count = cart.orders.map(post => post.productId).indexOf(data.products[index].productId);

// 									// if current product is not similar with the original cart, PUSH
// 									if(count === -1){

// 										cart.orders.push({productId: data.products[index].productId, productName: productName, quantity: data.products[index].quantity, price: productPrice, subTotal: subTotal});
// 									}
// 									// if not, splice it first, then push
// 									else{
// 										cart.orders.splice(count, 1);
// 										cart.orders.push({productId: data.products[index].productId, productName: productName, quantity: data.products[index].quantity, price: productPrice, subTotal: subTotal});

// 									}

// 									return cart.save();
// 								});

// 								index++;

// 								}
// 								// CLEAN THIS UP!
// 								await Cart.findOne({userId: data.userId}).then(cart =>{
// 									let subTotalList = [];
// 									for (let index = 0; index < data.products.length; index++){
// 										subTotalList.push(cart.orders[index].subTotal)
// 									};
// 									let totalAmount = 0;
// 									for (let i in subTotalList){
// 										totalAmount += subTotalList[i];
// 									};

									
// 									cart.totalAmount = totalAmount;
// 									currentCart = cart.orders;

									
// 									console.log(totalAmount);

// 									return cart.save();

// 								});

// 								return `Here are your cart details: \n \n ${currentCart}`;
// 						}
// 						else{
// 							return "Current inventory of one of the products chosen are insufficient. Please try again."
// 						};
			
// 			}
// 			else{
// 				return "The product or one of the product is inactive. Please try another product."
// 			}
// 		}
// 		else{
// 			return "Product/s can only be purchased by non-Admin users."
// 		}
// };



// DELETE ITEMS ON CART





// USER CHECKOUT [UPGRADED!!]

		// ADJUST INVENTORY WHEN PUSHED TO THE ORDERED PRODUCTS 

module.exports.checkout = async (data) => {
	if(data.isAdmin == false){
		Cart.findOne({userId: data.userId}).then(cart=>{

				console.log("data cart findone", cart);
				if(cart.orders.length == 0){
					return false; // NOT WORKING IDK WHY
				}
		////////////////////////////////////////////////////
				else{




/*					// THIS WORKS NOW!! (BUT CART SHOULD PUSH ALL ITS CONTENTS IN ONE GO, NOT ONE AT A TIME, BECAUSE TOTAL AMOUNT IS PUSHED BASED ON THE LENGTH OF THE CART)
					for (let index = 0; index < cart.orders.length; index++){

						productId = cart.orders[index].productId;
						productName = cart.orders[index].productName;
						quantity = cart.orders[index].quantity;
						price = cart.orders[index].price;
						subTotal = cart.orders[index].subTotal;
						totalAmount = cart.totalAmount;

						User.findById(data.userId).then(user => {
						
							user.orders.push({products: {productId: productId, productName: productName, quantity: quantity, price: price, subTotal: subTotal}, totalAmount: totalAmount});

							user.save();
						});
					};			

*/

					let finalCart = cart;

					User.findById(data.userId).then(user => {

						user.orders.push({products: finalCart.orders, totalAmount: finalCart.totalAmount});

						user.save();
					});


					// THIS WORKS!!
					for (let index = 0; index < cart.orders.length; index++){

						quantity = cart.orders[index].quantity;
						

						// ADJUST INVENTORY WHEN PUSHED TO THE ORDERED PRODUCTS 

						Product.findById(cart.orders[index].productId).then(product => {
							inventorySold = product.inventorySold;
							inventorySoldUpdated = inventorySold + quantity;
							product.orders.push({userId: data.userId, quantity: quantity});
							product.inventorySold = inventorySoldUpdated;

							product.save();
						})
					};


					// EMPTY THE CART AFTER THE ORDERS ARE PUSHED INSIDE THE PRODUCT/S AND THE USER
					Cart.findOne({userId: data.userId}).then(oldCart => {
						
							oldCart.orders = [];
						
							oldCart.save();

					});

					return true;
				}
		/////////////////////////////////////////
			
		})

		
		return true;
		
	}
	else{
		return false;
	}

};


