const express = require("express");
const router = express.Router();

const Cart = require("../models/cart.js");
const cartController = require("../controllers/cartController.js");

const User = require("../models/user.js");
const userController = require("../controllers/userController.js");

const auth = require("../auth.js");


// REGISTER NEW USER (SESSION 1)
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

// REGISTER ADMIN  (SESSION 1)
router.post("/registerAdmin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
});

// LOGIN USER (SESSION 1)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
});

// RETRIEVE ALL ORDERS - ADMIN ONLY (ADMIN ONLY)

router.get("/all", auth.verify, (req, res) => {
	const retrieve = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllOrders(retrieve).then(result => res.send(result))
});

router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
});


/*//RETRIEVE USER DETAILS BY ID (SESSION 4)
router.get("/userDetails/:id", (req, res) => {
	userController.getProfile(req.params.id).then(result => res.send(result))
});
*/

router.get("/details", auth.verify, (req, res) => {
	const userData = {
		id: auth.decode(req.headers.authorization).id
	}
		
	userController.getProfile(userData).then(resultFromController => res.send(resultFromController))
});

// USER CHECKOUT - NON-ADMIN (SESSION 4)
router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: []
	};

	for (let index = 0; index < req.body.products.length; index++){
		data.products.push({productId: req.body.products[index].productId, quantity: req.body.products[index].quantity})
	}
	
	userController.order(data).then(result => res.send(result))
});

// MAKE USER AS ADMIN - ADMIN ONLY (STRETCH GOAL)

router.patch("/setAdmin/:id", auth.verify, (req, res) => {
	const update = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.makeUserAsAdmin(req.params.id, update).then(result => res.send(result))
});

//RETRIEVE AUTHENTICATED USER'S ORDERS BY ID (STRETCH GOAL)
router.get("/myOrders", (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.retrieveOrders(data).then(result => res.send(result))
});







module.exports = router;