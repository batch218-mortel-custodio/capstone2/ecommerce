
const express = require("express");
const router = express.Router();

const Product = require("../models/product.js");

const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


// CREATE PRODUCT -ADMIN ONLY (SESSION 2)
router.post("/create", (req, res) => {
	const data = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		initialInventory: req.body.initialInventory
	}
	productController.addProduct(data).then(result => res.send(result))
});




// RETRIEVE ALL ACTIVE PRODUCT - ALL USERS (SESSION 2)
router.get("/active", (req, res) => {
	productController.getActiveProduct().then(result => res.send(result))
});

router.get("/all", (req, res) => {
	productController.getAllProduct().then(result => res.send(result))
});





// RETRIEVE SINGLE PRODUCT BY ID - ALL USERS (SESSION 3)
router.get("/:id", (req, res) => {
	productController.getProduct(req.params.id).then(result => res.send(result))
});









router.patch("/update/:id", (req, res) => {
	const data = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		initialInventory: req.body.initialInventory
	}
	productController.updateProduct(req.params.id, data).then(result => res.send(result))
});


// UPDATE PRODUCT INFO BY ID - ADMIN ONLY (SESSION 3)
/*router.patch("/update/:productId", auth.verify, (req, res) => {
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(req.params.productId, newData).then(result => res.send(result))
});
*/

// ARCHIVE A PRODUCT BY ID - ADMIN ONLY (SESSION 3)

router.patch("/archive/:id", (req, res) => {
	productController.archiveProduct(req.params.id).then(result => res.send(result))
});

router.patch("/activate/:id", (req, res) => {
	productController.activateProduct(req.params.id).then(result => res.send(result))
});


/*router.patch("/archive/:productId", auth.verify, (req, res) => {
	const update = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params.productId, update).then(result => res.send(result))
});

*/




module.exports = router;