const express = require("express");
const router = express.Router();

const Cart = require("../models/cart.js");
const cartController = require("../controllers/cartController.js");

const auth = require("../auth.js");






// //ADD TO CART / MODIFY QUANTITY (STRETCH GOAL)
// router.post("/addtocart", auth.verify, (req, res) => {
// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 		products: []
// 	};

// 	for (let index = 0; index < req.body.length; index++){
// 		data.products.push({productId: req.body[index].productId, quantity: req.body[index].quantity})
// 	}
	
// 	cartController.addToCart(data).then(result => res.send(result))
// });

//ADD TO CART / MODIFY QUANTITY (STRETCH GOAL)
router.post("/addtocart", auth.verify, (req, res) => {
	console.log("reqbody addTOCART sa cart Route", req.body);
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	};
	
	cartController.addToCart(data).then(result => res.send(result))

	// res.send(result) courtesy of Sir Enrico
});


//REMOVE PRODUCT FROM CART (STRETCH GOAL)
router.post("/removefromcart", auth.verify, (req, res) => {
	console.log("reqbody ni remove", req.body);
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	};
	
	cartController.removeFromCart(data).then(result => res.send(result))

	// res.send(result) courtesy of Sir Enrico
});

// RETRIEVE CART

router.get("/", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	cartController.retrieveCart(data).then(result => res.send(result))
});




// DELETE ITEM/S FROM CART



// CHECKOUT
router.post("/checkout", (req, res) => {
		let data = {
			userId: auth.decode(req.headers.authorization).id,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}
		cartController.checkout(data).then(result => res.send(result))
});








module.exports = router;