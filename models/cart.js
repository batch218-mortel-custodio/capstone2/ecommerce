const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({

    userId: {
    	type: String,
    	required: [true, "UserId is required"]
    },
    orders: [
      {
        productId: {
        	type: String,
        	required: [true, "ProductId is required"]
        },
        productName: {},
        quantity: {
        	type: Number,
        	required: [true, "ProductId is required"]
        },
        price: {},
        subTotal: {}
      }
    ],
    totalAmount: {
      type: Number,
      default: 0
    },
  	modifiedOn: {
  		type: Date,
  		default: new Date()
  	},
  	status :{
  		type: String,
  		default: "Pending"
  	}
});


module.exports = mongoose.model("Cart", cartSchema);