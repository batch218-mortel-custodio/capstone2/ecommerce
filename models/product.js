const mongoose = require("mongoose");
const { Schema } = mongoose;

const productSchema = Schema({
		name :{
			type: String,
			required: [true, "Course is required"]
		},
		description :{
			type: String,
			required: [true, "Description is required"]
		},
		price :{
			type: Number,
			required: [true, "Price is required"]
		},
		initialInventory:{
			type: Number,
			required: [true, "Inventory is required"]
		},
		inventorySold:{
			type: Number,
			default: 0
		},
		isActive :{
			type: Boolean,
			default: true
		},
		createdOn :{
			type: Date,
			default: new Date()
		},
		orders : [
			{	
					userId:{
						type: String,
						required: [true, "UserId is required"]
					},
					quantity:{
						type: Number,
						required: [true, "UserId is required"]
					},
					purchasedOn: {
						type: Date,
						default: new Date()
					}
			}
		]
	})



module.exports = mongoose.model("Product", productSchema);