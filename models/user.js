const mongoose = require("mongoose");
const Product = require("../models/product.js");

const { Schema } = mongoose;

const userSchema = Schema({
		firstName :{
			type: String,
			required: [true, "First name is required"]
		},
		lastName :{
			type: String,
			required: [true, "Last name is required"]
		},
		email :{
			type: String,
			required: [true, "Email is required"]
		},
		password :{
			type: String,
			required: [true, "Password is required"]
		},
		isAdmin :{
			type: Boolean,
			default: false
		},
		mobileNo :{
			type: String,
			required: [true, "Mobile Number is required"]
		},
		orders : [{
			products:[],
			totalAmount: {},
			purchasedOn: {	
				type: Date,
				default: new Date()
			},
			status :{
				type: String,
				default: "Purchased"
			}
		}]
});



								//modelName   //schema
module.exports = mongoose.model("User", userSchema);

/*
						products : [{
							productId:{
								type: String,
								required: [true, "ProductId is required"]
							},
							productName:{},
							quantity:{
								type: Number,
								required: [true, "ProductId is required"]
							},
							price:{},
							subTotal: {}
						}],

						totalAmount: {},

						purchasedOn: {	
							type: Date,
							default: new Date()
						},

						status :{
							type: String,
							default: "Purchased"
						}
*/