const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoute.js");
const productRoutes = require("./routes/productRoute.js");
const cartRoutes = require("./routes/cartRoute.js");

//////////////////////////////////////////////

// Server Setup
const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/users/cart", cartRoutes);


mongoose.connect("mongodb+srv://admin:admin@tropicalhut.1jg57fj.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);


mongoose.connection.once("open", () => console.log("Now connected to Custodio-MongoDB Atlas."));

app.listen(process.env.PORT || 5000, () => {console.log(`Server running at port ${process.env.PORT || 5000}`)});